# Процесс сдачи домашней работы #

## Подготовка
* [Создаём репозиторий](NewProject.md)
* [Структура каталогов](Hierarchy.md)
* [Подключаем ревьюверов](AddReviewrs.md)

## Процесс ревью

### Создание Pull Request
Используем технологию Feature Branch

* [Документация от Atlassian](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow)
* [Git handbook](https://git-scm.com/book/ch3-2.html)
* [Русский перевод](https://habrahabr.ru/post/106912/): используем только feature branches + master

1. Фактически оформляем решение задачи как feature branch
2. На разные задачи делаем разные бранчи
3. Задача считается прошедшей кодревью, когда Pull Request с этой задачей был замерджен в мастер

Скринкаст создания Pull Request:

[![Video](http://img.youtube.com/vi/bAtRwvsu71Y/0.jpg)](https://www.youtube.com/watch?v=bAtRwvsu71Y)


### Требования к решению
1. google cpp [style guide](https://google.github.io/styleguide/cppguide.html)
2. Декомпозиция программ на файлы
3. Декомпозиция программ на классы
4. Юниттесты [gtest + gmock](https://github.com/google/googletest) [rus](https://habrahabr.ru/post/119090/)