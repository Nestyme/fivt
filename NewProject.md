# Создание нового проекта #


### Жмём создать новый репозиторий 
![Alt text](screenshot-2016-10-05-20-29-18.7JzV.png)
### Заполняем название и не забываем галку "This is a private repository"
![Alt text](screenshot-2016-10-05-20-29-57.b8iI.png)
### Выполняем инструкцию по инициализации git репозитория
![Alt text](screenshot-2016-10-05-20-30-26.GEMf.png)
